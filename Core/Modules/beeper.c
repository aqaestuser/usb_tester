/*
 * beeper.c
 *
 *  Created on: Jan 21, 2022
 *      Author: KARMA
 */

#include "beeper.h"

#define _TIMER_PSC			(4)
#define _TIMER_FREQUENCY	(72000000 / _TIMER_PSC)

static TIM_HandleTypeDef * _htim;


void BEEPER_Init(TIM_HandleTypeDef * htim)
{
	_htim = htim;
	_htim->Instance->PSC = _TIMER_PSC - 1;
	HAL_TIM_Base_Start(htim);
	HAL_TIMEx_PWMN_Start(htim, TIM_CHANNEL_2);
}

void BEEPER_Beep(uint32_t frequency)
{
	uint32_t timArr = _TIMER_FREQUENCY / frequency;
	if(timArr > 0xFFFF)
	{
		timArr = 0xFFFF;
	}

	_htim->Instance->ARR  = timArr;
	_htim->Instance->CCR2 = timArr / 2;
}

void BEEPER_Off(void)
{
	_htim->Instance->CCR2 = 0;
}



