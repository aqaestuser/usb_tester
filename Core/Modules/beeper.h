/*
 * beeper.h
 *
 *  Created on: Jan 21, 2022
 *      Author: KARMA
 */

#ifndef BEEPER_H_
#define BEEPER_H_

#include "stm32f1xx_hal.h"

void BEEPER_Init(TIM_HandleTypeDef * htim);
void BEEPER_Beep(uint32_t frequency);
void BEEPER_Off(void);

#endif
